// get the client
const mysql = require('mysql');

//
// For more information about mysql library, check
// https://www.npmjs.com/package/mysql
//
const dbconfig = {
  host: process.env.DB_HOST || 'localhost',
  user: process.env.DB_USER || 'gamedb_user',
  database: process.env.DB_DATABASE || 'gamedb',
  password: process.env.DB_PASSWORD,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
  debug: false
};

const pool = mysql.createPool(dbconfig);

console.log(`Connected to database '${dbconfig.database}' on host '${dbconfig.host}'`);

module.exports = pool;
