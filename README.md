# Avans Hogeschool Breda - Programmeren 4 example project

Dit project is voorbeeldcode die we gebruiken in de lessen Programmeren 4 bij de opleiding informatica van Avans Hogeschool in Breda.

Een online versie van deze voorbeeldserver draait op [Heroku](https://www.heroku.com) op [https://node-gameserver.herokuapp.com](https://node-gameserver.herokuapp.com/api/games)

## Vereisten

Om deze applicatie te kunnen bouwen en runnen heb je de volgende software nodig:

- [Nodejs](https://nodejs.org), naar keuze de long term support (LTS) versie of laatste versie.
- [MySQL Community version](https://dev.mysql.com/downloads/), of via [XAMPP](https://www.apachefriends.org/download.html)

Start de database server en importeer het `database.sql` script.

## Installatie

Voer de volgende commands uit:

```
npm install
npm start
```

## Tests

Voer de testcases uit door het volgende commando te runnen:

```
npm run test
```

Dit start het test-script in `package.json`. Zoals je in dat script kunt zien wordt ook code coverage informatie gegenereerd. Dit is informatie over de dekking van de code door de testcases. Deze dekking wordt gezien als een indicatie van de kwaliteit van je code.

> Voor het uitvoeren van de testcases is toegang tot de bijbehorende database benodigd. Wanneer je test op je lokale machine moet de databaseserver dus draaien, en het databasescript geïmporteerd zijn.

## SonarQube analyse

Dit project ondersteunt het genereren van metrieken voor SonarQube. Sonar analyseert je project, bekijkt eventueel de code coverage die uit de testcases volgt, en stuurt die metrieken naar een online server. De metrieken voor dit project vind je op [https://sonarqube.avans-informatica-breda.nl](https://sonarqube.avans-informatica-breda.nl/dashboard?id=node_gameserver).

Om een analyse te genereren run je de volgende commands:

```
npm run test	// genereert code coverage in de folder './coverage'
npm run sonar
```

Om deze analyse voor je eigen project uit te voeren pas je het bestand [sonar-project.properties](https://gitlab.com/avans-informatica-breda/programmeren/programmeren-4/node-gameserver/blob/master/sonar-project.properties) aan. Let op dat je eventueel de programmeertaal die geanalyseerd wordt in de properties aanpast! Neem ook het `sonar` script uit [package.json](https://gitlab.com/avans-informatica-breda/programmeren/programmeren-4/node-gameserver/blob/master/package.json#L13) over, en installeer de benodigde devDependency.

# Gebruikte packages

De packages die we gebruiken:

- [Express](https://expressjs.com/)
- [Mocha](https://mochajs.org/) en [Chai](https://www.chaijs.com/)
- [husky](https://www.npmjs.com/package/husky)
- [pretty-quick](https://www.npmjs.com/package/pretty-quick)
- [Sonar scanner](https://www.npmjs.com/package/sonarqube-scanner)
