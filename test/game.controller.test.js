const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const pool = require('../src/config/db');

chai.should();
chai.use(chaiHttp);

const endpointToTest = '/api/games';

const CLEAR_DB = 'DELETE IGNORE FROM `games`';
const INSERT_QUERY =
  'INSERT INTO `games` (`title`, `producer`, `year`, `type`) VALUES ' +
  "('Battlefield 5', 'EA', 2018, 'FIRST_PERSON_SHOOTER')," +
  "('Fortnite', 'Epic Games', 2017, 'THIRD_PERSON_SHOOTER')," +
  "('Minecraft', 'Mojang', 2009, 'ADVENTURE');";

beforeEach(done => {
  pool.query(CLEAR_DB, (err, rows, fields) => {
    if (err) {
      console.log(`beforeEach CLEAR error: ${err}`);
      done(err);
    } else {
      pool.query(INSERT_QUERY, (err, rows, fields) => {
        if (err) {
          console.log(`beforeEach INSERT error: ${err}`);
          done(err);
        } else {
          console.log(`beforeEach: ${rows.message}`);
          done();
        }
      });
    }
  });
});

after(done => {
  pool.query(CLEAR_DB, (err, rows, fields) => {
    if (err) {
      console.log(`after error: ${err}`);
      done(err);
    } else {
      console.log(`after:`, rows);
      done();
    }
  });
});

describe('Games API GET', () => {
  it('should return an array of Games', done => {
    chai
      .request(server)
      .get(endpointToTest)
      .end((err, res) => {
        assert.strictEqual(err, null);
        res.should.have.status(200);
        res.body.should.be.an('object');
        res.body.should.have.property('result').that.is.an('array');

        const games = res.body.result;
        games.should.have.length(3);

        const game1 = games[0];
        console.log(game1);
        game1.should.have.property('ID').that.is.a('number');

        game1.should.have
          .property('title')
          .that.is.a('string')
          .that.equals('Battlefield 5');

        game1.should.have.property('producer').that.is.a('string');

        game1.should.have
          .property('year')
          .that.is.a('number')
          .that.equals(2018);

        done();
      });
  });
});

describe('Games API POST', () => {
  it.skip('should return a valid game when posting a valid object', done => {
    chai
      .request(server)
      .post(endpointToTest)
      .send({
        name: '  somename  ',
        producer: '  someproducer   ',
        year: 2020,
        type: ' sometype '
      })
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');

        const Games = res.body;
        Games.should.have.property('name').that.is.an('object');

        const name = Games.name;
        name.should.have.property('firstname').equals('FirstName');
        name.should.have.property('lastname').equals('LastName');
        Games.should.have.property('email').equals('user@host.com');
        Games.should.not.have.property('password');
        done();
      });
  });

  it.skip('should throw an error when using invalid JWT token', done => {
    chai
      .request(server)
      .post(endpointToTest)
      .set('x-access-token', 'in.valid.token')
      .send({
        name: '  somename  ',
        producer: '  someproducer   ',
        year: 2020,
        type: ' sometype '
      })
      .end((err, res) => {
        res.should.have.status(401);
        res.body.should.be.a('object');
        const error = res.body;
        error.should.have.property('message');
        error.should.have.property('code').equals(401);
        error.should.have.property('datetime');
        done();
      });
  });

  it.skip('should throw an error when no valid firstname is provided', done => {
    // Write your test here
    done();
  });

  it.skip('should throw an error when no lastname is provided', done => {
    // Write your test here
    done();
  });

  it.skip('should throw an error when no valid lastname is provided', done => {
    // Write your test here
    done();
  });
});

describe('Games API PUT', () => {
  it.skip('should return the updated Games when providing valid input', done => {
    const token = require('./authentication.test').token;
    // console.log('token = ' + token)
    chai
      .request(server)
      .put(endpointToTest + '/0')
      .set('x-access-token', token)
      .send({
        name: '  somename  ',
        producer: '  someproducer   ',
        year: 2020,
        type: ' sometype '
      })
      .end((err, res) => {
        // Check:
        // Verify that the Games that we get is the updated Games.
        res.should.have.status(200);
        res.body.should.be.a('object');

        const response = res.body;
        response.should.have.property('name').which.is.an('object');
        const name = response.name;
        name.should.have.property('firstname').equals('NewFirstName');
        name.should.have.property('lastname').equals('NewLastName');

        // Double check:
        // Send a GET-request to verify that the Games has been updated.
        chai
          .request(server)
          .get('/api/Gamess')
          .set('x-access-token', token)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.an('array');
            const result = res.body;
            result[0].name.should.have.property('firstname').equals('NewFirstName');
            result[0].name.should.have.property('lastname').equals('NewLastName');

            done();
          });
      });
  });
});

describe('Games API DELETE', () => {
  it.skip('should return http 200 when deleting a Games with existing id', done => {
    // Write your test here
    done();
  });
});
